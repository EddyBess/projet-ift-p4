/* Grille de Puissance 4 */  


#include <stdio.h>
#include <stdlib.h>
#include <efface_ecran.h>
#include <saisie.h>


int x;
int y;
int tableau;
int colonne = 0 ;
int ligne = 5 ;
int victoire = 0;
int j=0;
int main(void){
     efface_ecran();
      fprintf(stdout,"             -Puissance 4-\n\n\t\t");
        fprintf(stdout,"^Menu^\n\n");
        fprintf(stdout,"     Appuyer sur entrer pour commencer\n");
	getchar();
	efface_ecran();

    char Joueur1[10];
    char Joueur2[10];
    /*On demande aux joueurs quels sont leur pseudo et on les affiche*/
    fprintf(stdout , "Joueur 1, quel est votre pseudo?\n");
    scanf("%s",Joueur1);
    
    fprintf(stdout , "Joueur 2 quel est votre pseudo?\n");
    scanf("%s",Joueur2);
    /*On redonne le nom de chaque joueur qu'ils ont choisi*/
    fprintf(stdout, "Le pseudo du joueur n°1 est : %s\n", Joueur1);
    fprintf(stdout, "Le pseudo du joueur n°2 est : %s\n\n", Joueur2);
    
    /*Attribution des pions Rouges (R) et des pions Jaunes (J) à chaque joueur */
    fprintf(stdout, "%s, Vous aurez les pions n°1 (1)\n",Joueur1) ;
    fprintf(stdout, "%s, Vous aurez les pions n°2 (2)\n\n",Joueur2) ;


	/*Initialisation des elements de la grille à 0 */
	int tableau [7][6] = {{0,0,0,0,0,0},
			     {0,0,0,0,0,0},
			     {0,0,0,0,0,0},
			     {0,0,0,0,0,0},
			     {0,0,0,0,0,0},
			     {0,0,0,0,0,0}};
	
	
	/* Saisie de la colonne de jeu */
   
	while (victoire !=1){
  		fprintf(stdout,"Sur quelle colonne voulez vous placer votre pion? ");
    		colonne = (int)saisie_nombre_entier_court(0,6);
	        
		int ok = 0;
		ligne = 5 ;

		while(ok != 1)
		{
			if(j ==0){
				if(tableau[colonne][ligne] == 0)
				{
					tableau[colonne][ligne] = 1;
					ok = 1;
					j=1;
				}
				else
				{
					ligne--;
				}
			}
			if ( j == 1 ){
				if(tableau[colonne][ligne] == 0)
				{
					tableau[colonne][ligne] = 2;
					ok = 1;
					j=0;
				}
				else
				{
					ligne--;
				}
			}
			
	


		}
		/*Affichage du tableau */
		fprintf(stdout,"\n \n");
		efface_ecran();
        	printf("+---+---+---+---+---+---+---+\n");
		for(y=0; y<6; y++)
    	    	{
		printf("| ");
            	for(x=0; x<7; x++)
            	{
                	
			if (tableau[x][y] == 0)
				fprintf(stdout,"  | "); 
			else
				fprintf(stdout,"%d | ", tableau[x][y]);
 
            	}
            	fprintf(stdout,"\n");
		printf("+---+---+---+---+---+---+---+");
            	fprintf(stdout,"\n");

            
        		}
        	
        	/*regles de jeux*/
       		for(y=0; y<6; y++)
           	{
            	for(x=0; x<7; x++)
            	{
                	if(tableau[y][x] == 1 && tableau[y+1][x]==1 && tableau[y+2][x]==1 && tableau[y+3][x]==1){
                  
                                	fprintf(stdout,"%s a gagné !", Joueur1);
					fprintf(stdout,"\n\n");
					victoire = 1;
                            	}
             
             
             		if(tableau[y][x] == 1 && tableau[y][x+1] == 1 && tableau[y][x+2] == 1 && tableau[y][x+3] == 1){
               
                                	fprintf(stdout,"%s a gagné !", Joueur1); 
                               		fprintf(stdout,"\n\n"); 
					victoire = 1;
                                
                            	}

			if(tableau[y][x] == 2 && tableau[y][x+1] == 2 && tableau[y][x+2] == 2 && tableau[y][x+3] == 2){
                    
                                	fprintf(stdout,"%s a gagné !", Joueur2); 
                                	fprintf(stdout,"\n\n");
					victoire = 1; 
                             
                            	}
			if(tableau[y][x] == 2 && tableau[y+1][x] == 2 && tableau[y+2][x] == 2 && tableau[y+3][x] == 2){
                 
                                	fprintf(stdout,"%s a gagné !", Joueur2);  
                                	fprintf(stdout,"\n\n");
					victoire = 1;
                            	}

			if(tableau[y][x] == 1 && tableau[y+1][x+1]==1 && tableau[y+2][x+2]==1 && tableau[y+3][x+3]==1){
                  
                                	fprintf(stdout,"%s a gagné !", Joueur1);  
                                	fprintf(stdout,"\n\n");
					victoire = 1;
                            	}
             
             
             		if(tableau[y][x] == 1 && tableau[y+1][x-1] == 1 && tableau[y+2][x-2] == 1 && tableau[y+3][x-3] == 1){
               
                                	fprintf(stdout,"%s a gagné !", Joueur1); 
                               		fprintf(stdout,"\n\n"); 
					victoire = 1;
                                
                            	}
			
			if(tableau[y][x] == 2 && tableau[y+1][x+1]==2 && tableau[y+2][x+2]==2 && tableau[y+3][x+3]==2){
                  
                                	fprintf(stdout,"%s a gagné !", Joueur2);  
                                	fprintf(stdout,"\n\n");
					victoire = 1;
                            	}
             
             
             		if(tableau[y][x] == 2 && tableau[y+1][x-1] == 2 && tableau[y+2][x-2] == 2 && tableau[y+3][x-3] == 2){
               
                                	fprintf(stdout,"%s a gagné !", Joueur2); 
                               		fprintf(stdout,"\n\n"); 
					victoire = 1;
                                
                            	}






                  	}
                   	}


	
	}
	
	

        
}


//
//  main.c
//  Programme demande pseudo
//
//  Created by Florian Porte on 19/11/2019.
//  Copyright © 2019 Florian Porte. All rights reserved.
//

#include <stdio.h>
#include <stdlib.h>

int main (void)
{
    char Joueur1[10];
    char Joueur2[10];
    /*On demande aux joueurs quels sont leur pseudo et on les affiche*/
    fprintf(stdout , "Joueur 1, quel est votre pseudo?\n");
    scanf("%s",Joueur1);
    
    fprintf(stdout , "Joueur 2 quel est votre pseudo?\n");
    scanf("%s",Joueur2);
    /*On redonne le nom de chaque joueur qu'ils ont choisi*/
    fprintf(stdout, "Le pseudo du joueur 1 est : %s\n", Joueur1);
    fprintf(stdout, "Le pseudo du joueur 2 est : %s\n\n", Joueur2);
    
    /*Attribution des pions Rouges (R) et des pions Jaunes (J) à chaque joueur */
    fprintf(stdout, "%s, Vous aurez les pions Rouges (R)\n",Joueur1) ;
    fprintf(stdout, "%s, Vous aurez les pions Jaunes (J)\n\n",Joueur2) ;
}
